# mindcap
**mind**storm **cap**ture is a minimal note taking application.

It isn't visual but entirely text based.

## Usage
Running mindcap without a file will result in crashes when attempting to (w)rite.

(r)ead - outputs the notes in memory

(a)ppend - takes an input and adds it to notes in memory

(d)elete - takes a number input and deletes it from the notes in memory

(w)rite - writes the notes in memory to disk

(q)uit - self-explanatory.

When (d)eleting, the number you use will be the note number.

```
note 1
note 2
note 3
```

To delete the second note you input 2, not 1.

You then end up with

```
note 1
note 3
```

## How are my notes stored?
Stored in a plaintext document of the name you stored.

They'll be stored in a format similar to this:

```
Welcome to mindcap!|~##~|This is a note!|~##~|This is also a note!
```
