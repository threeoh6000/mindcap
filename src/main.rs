use std::env;
use std::fs;
use std::io;
use std::process;

fn read() -> String {
	let args: Vec<String> = env::args().collect();

	if args.len() == 1 {
		println!("Warning: no file passed!");
		println!("Program continuing but won't work as expected");
		println!("Here be dragons!");
		println!("");
		return "".to_string();
	}

	match fs::read_to_string(&args[1]) {
		Ok(x) => x.to_string(),
		Err(_) => "Welcome to mindcap!".to_string(),
	}

}

fn write(notes: &Vec<String>, splitter: &str) {
	let args: Vec<String> = env::args().collect();
	let raw_notes = notes.join(splitter);

	fs::remove_file(&args[1]);
	fs::write(&args[1], raw_notes.as_bytes());
}

fn arrayout(notes: &Vec<String>) {
	for x in notes {
		println!("{}", x)
	}
}

fn main() {
	let splitter = "|~##~|";
	let file = read();
	let raw_notes = file.split(&splitter);
	let mut notes: Vec<String> = vec![];
	let mut input;

	for x in raw_notes {
		let y = x.replace("\n", "");
		notes.push(y.to_string());
	}

	println!("MINDstorm CAPture by threeoh6000");
	println!("Enter your command.");

	loop {
		input = String::new();
		io::stdin().read_line(&mut input).expect("No input passed."); 

		let process = input.trim().to_string();
		
		if process == "r" {
			arrayout(&notes);
		} else if process == "a" {
			input = String::new();
			io::stdin().read_line(&mut input).expect("No input passed."); 

			let y = input.replace("\n", "");
			notes.push(y);

		} else if process == "d" {
			input = String::new();
			io::stdin().read_line(&mut input).expect("No input passed."); 

			let y = input.replace("\n", "").parse::<usize>().unwrap();
			notes.remove(y-1);
		} else if process == "w" {
			write(&notes, &splitter);
		} else if process == "q" {
			process::exit(0);
		}
	}

}
